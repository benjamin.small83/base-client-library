from base_client_library.factories.base import BaseFactory
from base_client_library.models.base import BaseModel
from base_client_library.exceptions import BaseException

class PokeException(BaseException):
    pass

class PokeApi(BaseFactory):

    base_url = "https://pokeapi.co/api/v2"

class Pokemon(BaseModel):
    pass

class PokemonFactory(PokeApi):

    path = "/pokemon"

    def get(self, name=None, pokemon_id=None):

        if name is None and id is None:
            raise PokeException("Pokemon.get requires either a name or pokemon_id to be passed")

        path = f"{self.path}/{name or pokemon_id}"

        resp = self.api_get_request(path)

        return Pokemon(**resp)

    def list(self, page_size=200):

        path = f"{self.path}/"

        init_resp = self.api_get_request(path=path, params={"limit": page_size})

        total_count = init_resp['count']

        for res in init_resp['results']:
            yield Pokemon(**res)

        for page_number in range(1, int(total_count / page_size) + 1):
            offset = page_number * page_size
            resp = self.api_get_request(path=path, params={"limit": page_size, "offset": offset})
            for res in resp['results']:
                yield Pokemon(**res)



if __name__ == "__main__":

    from pprint import pprint

    poke_list = []
    for pokemon in PokemonFactory(verify_ssl=False).list():
        poke_list.append(pokemon)

    poke_list

